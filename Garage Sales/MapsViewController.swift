//
//  MapsViewController.swift
//  Garage Sales
//
//  Created by Sucharu hasija on 24/11/17.
//  Copyright © 2017 KW SYSTEMS. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import NVActivityIndicatorView
import MapKit
import CoreLocation

class MapsViewController: UIViewController, NVActivityIndicatorViewable, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var adView: GADBannerView!
    
    let refProducts = Database.database().reference()
    var productInfoArray:[Sale] = []
    var userUID = Auth.auth().currentUser!.uid

    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        getListOfProducts()
        self.mapView.showsUserLocation = true
        
        adView.adUnitID = "ca-app-pub-0187834556320329/8322934772"
        adView.rootViewController = self
        adView.load(GADRequest())
        mapView.delegate = self
    }
    func getListOfProducts() {
        
        self.productInfoArray.removeAll()
        
        
        startAnimating()
        refProducts.child("Sales").queryOrderedByKey().observe(.childAdded, with: { snapshot in
            
            let prod = snapshot.value as! NSDictionary
            let active = snapshot.key
            let rejected = prod["NotInterested"] as! String
            var findit = false
            
            print(snapshot)
            self.stopAnimating()
            // print(rejected)
            
            if (rejected != self.userUID){
                //print(active)
                if rejected.contains(","){
                    let pointsArr = rejected.components(separatedBy: ",")
                    for x in pointsArr{
                        if x.trimmingCharacters(in: NSCharacterSet.whitespaces) == self.userUID {
                            // print("dont show")
                            findit = true
                            return
                        }
                    }
                    
                    if (findit == false){
                        
                            
                            
                            let prd = Sale(dict: prod, withProduct: snapshot.key)
                            self.productInfoArray.append(prd)
                            self.showPinsToMapView(with: prd)
                        
                    }
                }else{
                    print(active)
                    
                        
                        let prd = Sale(dict: prod, withProduct: snapshot.key)
                        self.productInfoArray.append(prd)
                        self.showPinsToMapView(with: prd)
                    
                }
                
            }
        })
        
        
    }

    func showPinsToMapView(with pro:Sale) {
        
        
   
            let pointAnnotation = MKPointAnnotation()
            pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: pro.latitude, longitude: pro.longitude)
            pointAnnotation.title = pro.address
            pointAnnotation.subtitle = pro.description
        
        
            self.mapView.addAnnotation(pointAnnotation)
        
        
        
        let latString = UserDefaults.standard.value(forKey: "latitude") as? String ?? "0.0"
        let longString = UserDefaults.standard.value(forKey: "longitude") as? String ?? "0.0"

        if let lat = Double(latString) , let long = Double(longString) {
            
            
            
            //let currentDestinationPointRect = MKMapRectMake(lat, long, 0, 0)
            
            self.mapView.setCenter(CLLocationCoordinate2DMake(lat, long), animated: true)
            

            
        }
        
        

        
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        
        if annotation is MKUserLocation {
            //return nil so map view draws "blue dot" for standard user location
            return nil
        }
        
        let reuseId = "pin"
        let  pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
        pinView.canShowCallout = true
        pinView.animatesDrop = true
        
        if #available(iOS 11, *) {
            // use UIStackView
            pinView.prepareForDisplay()
            
        } else {
            // show sad face emoji
        }
        
        pinView.pinTintColor = UIColor.darkGray
        pinView.isDraggable = true
        
        let btn = UIButton(type: .detailDisclosure)
        pinView.rightCalloutAccessoryView = btn
        
        return pinView
        
        
      //  return nil
        
    }
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        
        if let x =  view.annotation!.title! {
            
            
            print(x)
            
            for item in self.productInfoArray {
                
                if item.address == x {
                    
                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "SearchProductsViewController") as! SearchProductsViewController
                    vc.isComingForViewOnly = true
                    vc.updateInfo = item
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                    return
                }
                
                
                
            }
            
            
            
            
        }
        
        
      
        
    }
    

}
