//
//  ListProductsViewController.swift
//  Garage Sales
//
//  Created by Sucharu hasija on 24/11/17.
//  Copyright © 2017 KW SYSTEMS. All rights reserved.
//

import UIKit
import Firebase
import MessageUI
import Messages
import NVActivityIndicatorView
import CoreLocation

class ListProductsViewController: UIViewController, MFMailComposeViewControllerDelegate, NVActivityIndicatorViewable {
    
    
    @IBOutlet weak var adView: GADBannerView!
    let refProducts = Database.database().reference()
    var productInfoArray:[Sale] = []
    var userUID = Auth.auth().currentUser!.uid

    let kCellName = "ProductTableViewCell"
    let kCellIdentifier = "productCell"
    var myLocation:CLLocation? = nil
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
      
        
        // Register Table View Cells
        

        adView.adUnitID = "ca-app-pub-0187834556320329/8322934772"
        adView.rootViewController = self
        adView.load(GADRequest())
        
        self.myLocation = CLLocation(latitude: 0.0 , longitude: 0.0)
        
        
        TRLocationManager.swiftSharedInstance.getUserLocation { (_) in
            
            if let latString = UserDefaults.standard.value(forKey: "latitude") as? String, let longString = UserDefaults.standard.value(forKey: "longitude") as? String {
                
                if let lat = Double(latString) , let long = Double(longString) {
                    
                    self.myLocation = CLLocation(latitude: lat, longitude: long)
                    
                    
                }
                
                
                
                
            }
            
            
            
        }
        
        self.tableView.register(UINib(nibName: kCellName, bundle: nil), forCellReuseIdentifier: kCellIdentifier)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        self.getListOfProducts()
    }
    
    
    func sendMailToUser(with email:String) {
        
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            
            self.showWarningMessage(with: "Email Functionality is not setup in your phone.")
            return
        }
        
        
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.setToRecipients([email])
        composeVC.setSubject("Product Feedback")
        composeVC.setMessageBody("Hey! Here's my feedback.", isHTML: false)
        
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
        
        
    }
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    
    func callToUser(with number:String) {
        
        
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func getListOfProducts() {
        
        self.productInfoArray.removeAll()
        
        
        startAnimating()
        refProducts.child("Sales").queryOrderedByKey().observe(.childAdded, with: { snapshot in
            
            let prod = snapshot.value as! NSDictionary
            let active = snapshot.key
            let rejected = prod["NotInterested"] as! String
            var findit = false
            
            print(snapshot)
            self.stopAnimating()
            // print(rejected)
            
            if (rejected != self.userUID){
                //print(active)
                if rejected.contains(","){
                    let pointsArr = rejected.components(separatedBy: ",")
                    for x in pointsArr{
                        if x.trimmingCharacters(in: NSCharacterSet.whitespaces) == self.userUID {
                            // print("dont show")
                            findit = true
                            return
                        }
                    }
                    
                    if (findit == false){
                        
              
                    
                            let prd = Sale(dict: prod, withProduct: snapshot.key)

                            self.productInfoArray.append(prd)
                            self.sortTheListAsPerUserLocation()
                            
                        
                    }
                }else{
                    print(active)
                    

                        let prd = Sale(dict: prod, withProduct: snapshot.key)

                        self.productInfoArray.append(prd)
                        self.sortTheListAsPerUserLocation()
                        
                    
                }
                
            }
        })
    }
    
    func sortTheListAsPerUserLocation() {
        
        
        
        print(self.productInfoArray)
//        self.productInfoArray  =  self.productInfoArray.filter({ (s) -> Bool in
//            
//            
//            print(s.startDate, Date(), s.endDate, s.startDate.compare(Date()) == ComparisonResult.orderedAscending,s.endDate.compare(Date()) == .orderedDescending)
//        return s.startDate.compare(Date()) == ComparisonResult.orderedAscending && s.endDate.compare(Date()) == .orderedDescending
//        
//            
//        })
        
        if self.productInfoArray.count <= 1 {
             self.tableView.reloadData()
            
            return
            
        }
        if self.myLocation == nil { return }
        
        self.productInfoArray.sort { (s1, s2) -> Bool in
            
            let l1 = CLLocation(latitude: s1.latitude, longitude: s1.longitude)
            let l2 = CLLocation(latitude: s2.latitude, longitude: s2.longitude)
            
            
            return self.myLocation!.distance(from: l1) > self.myLocation!.distance(from: l2)
        }
        
        self.tableView.reloadData()
        
    }
    
    
}
extension ListProductsViewController:UITableViewDelegate, UITableViewDataSource {
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.productInfoArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:ProductTableViewCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier, for: indexPath) as! ProductTableViewCell
        
        cell.setupCellInformation(with: self.productInfoArray[indexPath.row], forIndex: indexPath.row)
        return cell
        
    }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            return 151.0
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "SearchProductsViewController") as! SearchProductsViewController
        
        vc.isComingForViewOnly = true
        vc.updateInfo = self.productInfoArray[indexPath.row]
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
        
    }
 
    
}
extension ListProductsViewController:ProductTableActionProtocol {
    
    func callPressedByUser(with ind:Int) {
        
        self.getUserInformation(isForEmail: false, withIndex: ind)
        
    }
    func emailPressedByUser(with ind:Int) {
        
        
        self.getUserInformation(isForEmail: true, withIndex: ind)
        
    }
    func getUserInformation(isForEmail:Bool, withIndex index:Int) {
        
        
        let findValueFor = isForEmail ? "Email":"phone"
        
        let userID = self.productInfoArray[index].userId
        
        print(userID, index, findValueFor)
        refProducts.child("Users").child(userID).child(findValueFor).observe(DataEventType.value, with: {
            
            (snapshot: DataSnapshot) in
            
            
            if isForEmail {
                
                
                self.sendMailToUser(with: snapshot.value  as! String)
                
            } else {
                
                
                self.callToUser(with: snapshot.value as! String)
                
            }
            
        })
    }
    
    
}
