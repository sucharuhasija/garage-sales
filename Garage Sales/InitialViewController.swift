//
//  InitialViewController.swift
//  Garage Sales
//
//  Created by Sucharu hasija on 24/11/17.
//  Copyright © 2017 KW SYSTEMS. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {

    @IBOutlet weak var garageSales: UIButton!
    @IBOutlet weak var products: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.garageSales.layer.masksToBounds = true
        self.garageSales.layer.cornerRadius = 6.0
        self.garageSales.layer.borderColor = UIColor.white.cgColor
        self.garageSales.layer.borderWidth = 3.0
        
        
        self.products.layer.masksToBounds = true
        self.products.layer.cornerRadius = 6.0
        self.products.layer.borderColor = UIColor.white.cgColor
        self.products.layer.borderWidth = 3.0
        
        
        
    }


}
