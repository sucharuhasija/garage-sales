//
//  SecondViewController.swift
//  Garage Sales
//
//  Created by Keneth Walters on 2017-11-02.
//  Copyright © 2017 KW SYSTEMS. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import GoogleMobileAds

import NVActivityIndicatorView


class SecondViewController: UIViewController, UITextViewDelegate,NVActivityIndicatorViewable, UINavigationControllerDelegate,  UIImagePickerControllerDelegate {
    
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    @IBOutlet weak var imageViewPhoto: UIImageView!
    
   
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var txtCategory: UITextField!
    
   
    
    var userUID = Auth.auth().currentUser!.uid
    
    var pickerView:UIPickerView!
    var categoryList:[String] = ["Buy and Sell","Services","Sales","Vacation Rentals","Community"]
    
    var imagePicker: UIImagePickerController!
    var imagPicker = UIImagePickerController()
    let refProducts = Database.database().reference()
    let refStorage = Storage.storage().reference()
    var isPhoneAvailable = false
    var isImageTakenToUpload = false

    
    let kEmailPhoneWarning = "Please update your email and Phone in profile section to add product in it."
    let kEnterProductNameWarning = "Please enter Product Name."
    let kEnterProductImageWarning = "Please enter Product Image."
    let kEnterProductPriceWarning = "Please enter Product Price."
    let kEnterProductCategoryWarning = "Please enter Product Category."

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        //
        //        addBannerViewToView(bannerView)
        //
        bannerView.adUnitID = "ca-app-pub-0187834556320329/7075579518"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        
        
        // Do any additional setup after loading the view, typically from a nib.
        textView.text = "Tell us something about!"
        textView.textColor = UIColor.lightGray
        textView.delegate = self
        
        
        imagPicker.delegate = self
        
        // Get User Location
        TRLocationManager.swiftSharedInstance.getUserLocation { (_) in
            
        }
        
        self.txtCategory.tag = 104
        self.txtCategory.delegate = self
        self.pickerView = UIPickerView()
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        if !isPhoneAvailable
        {
            
            self.checkForEmailAndPhone()
            
        }
        
        
    }
    
    
    
    

  
    @IBAction func btnSave(_ sender: Any) {
        
        
       
        
        
        if !self.isPhoneAvailable {
            
            
            let alert = UIAlertController(title: "Alert", message: kEmailPhoneWarning, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Go to profile", style: UIAlertActionStyle.default, handler: { (_) in
                
                self.tabBarController?.selectedIndex = 2
                
            })   )
            
            alert.addAction( UIAlertAction(title: "Cancel", style: .default, handler: nil)  )
            
            
            self.present(alert, animated: true)
            
            return
            
        } else if let  _ = Auth.auth().currentUser!.email {
            
        
            
            if self.txtName.text!.isEmpty {
                
                
                self.showWarningMessage(with: kEnterProductNameWarning)
                return
            } else if self.txtPrice.text!.isEmpty {
                
                
                self.showWarningMessage(with: kEnterProductPriceWarning)
                return
            } else if !isImageTakenToUpload {
                
                
                self.showWarningMessage(with: kEnterProductImageWarning)
                return
                
            }
            
            
         startAnimating()
        print("paso aqui tambien")
        
        let name = txtName.text
        let price = txtName.text
        let description = textView.text
        // let randomName = randomStringWithLength(length: 5)
        //  let randomNames = randomStringWithLength(length: 9)
        
        // let imageDatas = UIImageJPEGRepresentation(imageTwo, 1.0)
        
        let latitude = UserDefaults.standard.value(forKey: "latitude") as? String
        let longitude = UserDefaults.standard.value(forKey: "longitude") as? String
            
            
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let currentDate = Date()
        let someDateTime = formatter.string(from: currentDate)
        
        imageViewPhoto.image = imageViewPhoto.image
//        dismiss(animated: true, completion: nil)
        var data = NSData()
        data = UIImageJPEGRepresentation(imageViewPhoto.image!, 0.8)! as NSData
        // set upload path
        let filePath = "\(Auth.auth().currentUser!.uid)/\("userPhoto" + someDateTime)"
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"
        self.refStorage.child(filePath).putData(data as Data, metadata: metaData){(metaData,error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }else{
                //store downloadURL
                let downloadURL = metaData!.downloadURL()!.absoluteString
                //store downloadURL at database
                
                
                var product = [String:Any]()
                product["userID"] = self.userUID
                product["name"] = name!
                product["price"] = price!
                product["description"] = description!
                product["postDate"] = someDateTime
                product["photoURL"] = downloadURL
                product["NotInterested"] = self.userUID
                product["category"] = self.txtCategory.text!
                product["latitude"] = latitude
                product["longitude"] = longitude
                
                
    
                self.refProducts.child("Products").childByAutoId().setValue(product)
                
                self.stopAnimating()
                
                
                
                
                
            }
            
        }
        
        } else {
            
            
            self.showWarningMessage(with: kEmailPhoneWarning)
            
        }
            
    }
    
    func showSuccessMessage() {
        
        
        
        
    }
    
    

    
    func checkForEmailAndPhone() {
        
        
        
        print(self.userUID)
        self.refProducts.child("Users").child(self.userUID).child("phone").observe(DataEventType.value) { (snapshot) in
            
            
            print(snapshot.value ?? "No value exists")
            if let  x = snapshot.value , x as? NSObject != NSNull() {
                
                
            
            self.isPhoneAvailable = true
            UserDefaults.standard.set(true, forKey: "isPhoneNumberAvailable")
            UserDefaults.standard.synchronize()
            }
        }
    }
    

    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Placeholder"
            textView.textColor = UIColor.lightGray
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnTakePhoto(_ sender: Any) {
   
        // self.btnSave.setTitleColor(UIColor.white, for: .normal)
        //self.btnSave.isUserInteractionEnabled = true
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagPicker = UIImagePickerController()
            imagPicker.delegate = self
            imagPicker.sourceType = .camera
            present(imagPicker, animated: true, completion: nil)
            
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagPicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagPicker.allowsEditing = true
        self.present(imagPicker, animated: true, completion: nil)
        
        
        
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagPicker.dismiss(animated: true, completion: nil)
        
        isImageTakenToUpload = true
        
        imageViewPhoto.image = info[UIImagePickerControllerOriginalImage] as? UIImage
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .top,
                                relatedBy: .equal,
                                toItem: self.topLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
        
    }
    
}
// TextField Extension

extension SecondViewController: UITextFieldDelegate {
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 104 {
            
            textField.inputView = self.pickerView
            
            
        }
 
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag == 104 {
            
            
            
            
        }
        
        
    }
  
}
extension SecondViewController :UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.categoryList.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        self.txtCategory.text = self.categoryList[row]
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
       return  self.categoryList[row]
        
    }
    
    
}


