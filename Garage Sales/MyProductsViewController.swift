//
//  MyProductsViewController.swift
//  Garage Sales
//
//  Created by Keneth Walters on 2017-11-03.
//  Copyright © 2017 KW SYSTEMS. All rights reserved.
//

import UIKit
import Firebase

class MyProductsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var product = [Product] ()

    @IBOutlet weak var productInfo: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    let dbRef = Database.database().reference().child("Products")
    
    var name: String = ""
    var description1: String = ""
    
     var userUID = Auth.auth().currentUser!.uid
    
   
  
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return product.count
    }
    
   
    @IBAction func moveBack(_ sender: UIBarButtonItem) {
        
        dismiss(animated: true, completion: nil)
    }
    
  
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "myPrototypeCell", for: indexPath)
        
        let eachProduct = product[indexPath.row]
        
        cell.textLabel?.text = eachProduct.name
        cell.imageView?.loadImageWithUrlWithFullSize(urlString: eachProduct.imageUrl)
        let btn = UIButton(frame:CGRect(x:0,y:0,width:30.0,height:30.0))
        btn.setImage(#imageLiteral(resourceName: "garbage"), for: [])
        btn.tag = indexPath.row + 1
        btn.addTarget(self, action: #selector(deletePressedByUser(_:)), for: UIControlEvents.touchUpInside)
        
        cell.accessoryView = btn
        
        
        
       // let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "myPrototypeCell" )
      //  addProducts()
        
       // cell.textLabel?.text = description1 + name
        // self.tableView.reloadData()
        //  return cell
        
        return cell
    }
    
    @objc func deletePressedByUser(_ sender:UIButton) {
        
        print(sender.tag)
        
        let alert = UIAlertController(title: "Confirm!", message: "Do you want to delete this product?. It will be permanently deleted from all.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes! Delete", style: UIAlertActionStyle.default, handler: { (_) in
            
            self.deleteProduct(with: sender.tag - 1)
        }) )
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true)
        
        
        
        
    }
    
    
    func deleteProduct(with tag:Int) {
        
        
        Database.database().reference().child("Products").child(self.product[tag].productKey).removeValue { (err,_) in
            
            if err == nil {
                
                let alert = UIAlertController(title: "Success!", message: "Your product is deleted successfully.", preferredStyle: .alert)
              
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true)
                
                
            }
            
            
        }
        
        self.tableView.reloadData()
        
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        
        
        print(indexPath.row)
        
        
        
        
    }
    
    
    func addProducts(){
        dbRef.observe(DataEventType.childAdded, with: {
            
            (snapshot: DataSnapshot) in
            
            self.description1 = snapshot.childSnapshot(forPath: "description").value as! String
            self.name = snapshot.childSnapshot(forPath: "name").value as! String
            
            
           // print(self.description1 + self.name)
            
        
        })
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
       
        

        //Needed to load the Data with observerMessages and put it into our Array: messages and later into the TableViewController
        observeProducts
        {
                (product) in
                
                self.product.insert(product, at: 0)
                self.tableView.reloadData()
        }
        
       //addProducts()
        
        
       
        // Do any additional setup after loading the view.
    }
    
    func observeProducts(withHandler handler: @escaping (_ Product: Product) -> ())
    {
       Database.database().reference().child("Products").queryOrderedByKey().observe(.childAdded, with:
            {
                (snapshot) in
                
                
                
                let dict = snapshot.value as! NSDictionary
                
                if let id = dict.value(forKey: "userID") as? String, id == self.userUID {
                    
                    guard let name = dict["name"] as? String,
                        let description = dict["description"] as? String,
                        let price = dict["price"] as? String,
                        let imageUrl = dict["photoURL"] as? String
                        else
                    {
                        print("Error in msg")
                        return
                    }
                    
                    let msgObj = Product(name: name, price: price, description: description,imageUrl:imageUrl,productKey:snapshot.key)
                    
                    handler(msgObj)
                    
                    
                    
                }
              
        })
    }


}
