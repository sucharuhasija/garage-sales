//
//  FirstViewController.swift
//  Garage Sales
//
//  Created by Keneth Walters on 2017-11-02.
//  Copyright © 2017 KW SYSTEMS. All rights reserved.
//

import UIKit
import Firebase
import Koloda
import pop
import NVActivityIndicatorView
import MessageUI
import CoreLocation

private let numberOfCards: Int = 5
private let frameAnimationSpringBounciness: CGFloat = 9
private let frameAnimationSpringSpeed: CGFloat = 16
private let kolodaCountOfVisibleCards = 2
private let kolodaAlphaValueSemiTransparent: CGFloat = 1.0

class FirstViewController: UIViewController, NVActivityIndicatorViewable,MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    
    var texto = ""
    var availableKeys: NSMutableDictionary = [:]
    var rejectedKeys: NSMutableDictionary = [:]
    
    @IBOutlet weak var kolodaView: KolodaView!
    
    let refProducts = Database.database().reference()
    let refStorage = Storage.storage().reference()
    
    var ProductId = ""
    var userUID = Auth.auth().currentUser!.uid
    var listProductsBad: [String] = []
    var listProductsGood: [String] = []
    
    var productInfoArray:[Product] = []
    var productFirebaseKeys:[String] = []
    var images:[UIImage] = []
    
    @IBOutlet weak var productPhoto: UIImageView!
    
   @IBOutlet weak var swipeLabel: UILabel!
    
    var picArray: [UIImage] = []
    var productImageViewArray:[UIImageView] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        self.navigationController!.navigationBar.prefersLargeTitles = true
        
        
        // addBannerViewToView(bannerView)
//        if #available(iOS 11, *) {
//            let guide = self.view.safeAreaLayoutGuide.topAnchor
//            let height = (self.navigationController?.navigationBar.frame.height)! - CGFloat(12)
//            
//            
//          
//        }
        
        bannerView.adUnitID = "ca-app-pub-0187834556320329/8322934772"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        // bannerView.delegate = self
        
        self.productPhoto.isHidden = true
        self.kolodaView.backgroundColor = .clear
        print(userUID)
//        let gesture = UIPanGestureRecognizer(target: self, action: #selector(wasDragged(gestureRecognizer:)))
//        productPhoto.addGestureRecognizer(gesture)
//
        kolodaView.dataSource = self
        kolodaView.delegate = self
        kolodaView.alphaValueSemiTransparent = kolodaAlphaValueSemiTransparent
        kolodaView.countOfVisibleCards = kolodaCountOfVisibleCards

        kolodaView.animator = BackgroundKolodaAnimator(koloda: kolodaView)
        
        TRLocationManager.swiftSharedInstance.getUserLocation { (_) in
            
        }
        
       // self.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
        
        createListProductsGood { value, error in
            guard let value = value, error == nil else {
                // present an alert and pass the error message
                if let error = error  {
                    print(error)
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Alert", message: error.description, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default))
                        self.present(alert, animated: true)
                    }
                }
                return
            }
            DispatchQueue.main.async {
                print(value)
            }
        }
        
            
       

    }
    
    //MARK: IBActions
    @IBAction func leftButtonTapped() {
    
        self.getUserInformation(isForEmail: false)
        
        
    }
    
    @IBAction func rightButtonTapped() {
        self.getUserInformation(isForEmail: true)
    
    }
    
    @IBAction func dismissVC(_ sender: UIBarButtonItem) {
        
        self.tabBarController?.dismiss(animated: true, completion: nil)
        
    }
    
    func getUserInformation(isForEmail:Bool) {
        
    
        let findValueFor = isForEmail ? "Email":"phone"
        let indx = self.kolodaView.currentCardIndex
        let userID = self.productInfoArray[indx].userId
        
        print(userID, indx, findValueFor)
        refProducts.child("Users").child(userID).child(findValueFor).observe(DataEventType.value, with: {
            
            (snapshot: DataSnapshot) in
            
            
            if isForEmail {
                
                
                self.sendMailToUser(with: snapshot.value  as! String)
                
            } else {
                
                
                self.callToUser(with: snapshot.value as! String)
                
            }
    
        })
    }
    
    
    
    func sendMailToUser(with email:String) {
        
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            
            self.showWarningMessage(with: "Email Functionality is not setup in your phone.")
            return
        }
        
        
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.setToRecipients([email])
        composeVC.setSubject("Product Feedback")
        composeVC.setMessageBody("Hey! Here's my feedback.", isHTML: false)
        
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
        
        
    }
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }

    
    
    func callToUser(with number:String) {
        
        
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    
    
    @objc func wasDragged(gestureRecognizer: UIPanGestureRecognizer) {
        let labelPoint = gestureRecognizer.translation(in: view)
        productPhoto.center = CGPoint(x: view.bounds.width / 2 + labelPoint.x, y: view.bounds.height / 2 + labelPoint.y)
        
        let xFromCenter = view.bounds.width / 2 - productPhoto.center.x
        
        var rotation = CGAffineTransform(rotationAngle: xFromCenter / 200)
        
        let scale = min(100 / abs(xFromCenter), 1)
        
        var scaledAndRotated = rotation.scaledBy(x: scale, y: scale)
        
        productPhoto.transform = scaledAndRotated
        
        if gestureRecognizer.state == .ended {
        
            var acceptedOrRejected = ""
            
            if productPhoto.center.x < (view.bounds.width / 2 - 100) {
                print("Not Interested")
                acceptedOrRejected = "rejected"
                setAcceptedOrRejected()
              
                createListProductsGood { value, error in guard let value = value, error == nil else {
                    // present an alert and pass the error message
                    return }
                    DispatchQueue.main.async { print(value) 
                        
                    } }
                
             }
            if productPhoto.center.x > (view.bounds.width / 2 + 100) {
                print("Interested")
                acceptedOrRejected = "accepted"
            }
            
            rotation = CGAffineTransform(rotationAngle: 0)
            
            scaledAndRotated = rotation.scaledBy(x: 1, y: 1)
            
            productPhoto.transform = scaledAndRotated
            
            productPhoto.center = CGPoint(x: view.bounds.width / 2, y: view.bounds.height / 2)
        }
    }

    func setAcceptedOrRejected() {
        refProducts.child("Products").queryOrderedByKey().observe(.childAdded, with: { snapshot in
            
            let prod = snapshot.value as! NSDictionary
    
            if self.ProductId == snapshot.key{
                
                self.texto = prod["NotInterested"] as! String
                self.refProducts.child("Products").child(self.ProductId).updateChildValues(["NotInterested": self.texto + ", " + self.userUID])
                
            } })
    }
    
    enum MyError: Error, CustomStringConvertible {
        case unknownError
        var description: String {
            switch self {
            case .unknownError: return "An unknow error occurred while fetching the image"
            }
        }
    }
    
   func createListProductsGood(finished: @escaping(_ value: Int?, MyError?) -> Void) {
    
    let value: Int? = nil
    let _: MyError = .unknownError
        
        self.productInfoArray.removeAll()
        productFirebaseKeys.removeAll()
        self.kolodaView.reloadData()
        startAnimating()
        refProducts.child("Products").queryOrderedByKey().observe(.childAdded, with: { snapshot in
            
            let prod = snapshot.value as! NSDictionary
            let active = snapshot.key
            let rejected = prod["NotInterested"] as! String
            let photoURL = prod["photoURL"] as! String
            var findit = false
            
            print(snapshot)
            self.stopAnimating()
            // print(rejected)
            
            if (rejected != self.userUID){
                //print(active)
                if rejected.contains(","){
                    let pointsArr = rejected.components(separatedBy: ",")
                    for x in pointsArr{
                        if x.trimmingCharacters(in: NSCharacterSet.whitespaces) == self.userUID {
                            // print("dont show")
                            findit = true
                            return
                        }
                    }
                    
                    if (findit == false){
                        if let url = NSURL(string: photoURL) {
//                            if let data = NSData(contentsOf: url as URL) {
//                                self.ProductId = active // REMOVE
//                                self.productPhoto.image = UIImage(data: data as Data) // REMOVE
//                                self.images.append(self.productPhoto.image!)
//
//
//
//                             //   finished(value, nil) //ADD
//                            }
                            let imageView = UIImageView()
                            imageView.loadImageWithUrlWithFullSize(urlString:photoURL)
                            self.productImageViewArray.append(imageView)
                            
                            let prd = Product(dict: prod, withProduct: snapshot.key)
                            self.productInfoArray.append(prd)
                            self.productFirebaseKeys.append(active)
                            self.sortListForLocation()
                        }
                    }
                }else{
                    print(active)
                    if let url = NSURL(string: photoURL) {
//                        if let data = NSData(contentsOf: url as URL) {
//                           self.ProductId = active // REMOVE
//                           self.productPhoto.image = UIImage(data: data as Data) // REMOVE
//                            self.images.append(self.productPhoto.image!)
//
//                        }else{
//                           // finished(value, " something went wrong!!") //ADD
//
//                        }
                        
                        
                        let imageView = UIImageView()
                        imageView.loadImageWithUrlWithFullSize(urlString:photoURL)
                        self.productImageViewArray.append(imageView)
                        self.productFirebaseKeys.append(active)
                        let prd = Product(dict: prod, withProduct: snapshot.key)
                        self.productInfoArray.append(prd)
                       self.sortListForLocation()
                    }
                }
                
            }
        })
        finished(value, nil)
    
        }
    
    
    func sortListForLocation() {
        
        if let myLocationLatitudeString = UserDefaults.standard.value(forKey: "latitude") as? String, let myLocationLongitudeString = UserDefaults.standard.value(forKey: "latitude") as? String {
            
            
            let myLat = Double(myLocationLatitudeString) ?? 0.0
            let myLong = Double(myLocationLongitudeString) ?? 0.0
            
            
            let myLocation = CLLocation(latitude: myLat, longitude: myLong)
            
            
            self.productInfoArray.sort(by: { (p1, p2) -> Bool in
                
                
                if let p1Lat = Double(p1.latitude), let p1Long = Double(p1.longitude),let p2Lat = Double(p2.latitude), let p2Long = Double(p2.longitude) {
                    
                    
                    let location1 = CLLocation(latitude: p1Lat, longitude: p1Long)
                    let location2 = CLLocation(latitude: p2Lat, longitude: p2Long)
                    
                    return myLocation.distance(from: location1) > myLocation.distance(from: location2)
                    
                }
                
                
                return false
            })
            
            self.kolodaView.reloadData()
            
        }
        
        
        
        
    }
    }





//MARK: KolodaViewDelegate
extension FirstViewController: KolodaViewDelegate {
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
      //  kolodaView.resetCurrentCardIndex()
    }
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
       // UIApplication.shared.openURL(URL(string: "https://yalantis.com/")!)
    }
    func koloda(_ koloda: KolodaView, shouldDragCardAt index: Int) -> Bool {
        
        
        return true
    }
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        
        if direction == SwipeResultDirection.left {
            self.ProductId = productFirebaseKeys[index]
            self.setAcceptedOrRejected()
            
        } else {
            
            
        }
        
        
    }
    
    
    func kolodaShouldApplyAppearAnimation(_ koloda: KolodaView) -> Bool {
        return false
    }
    
    func kolodaShouldMoveBackgroundCard(_ koloda: KolodaView) -> Bool {
        return false
    }
    
    func kolodaShouldTransparentizeNextCard(_ koloda: KolodaView) -> Bool {
        return true
    }
    func kolodaSwipeThresholdRatioMargin(_ koloda: KolodaView) -> CGFloat? {
        
        return 0.8
    }
    
    func kolodaDidResetCard(_ koloda: KolodaView) {
        
        
        let range = self.kolodaView.currentCardIndex...self.kolodaView.countOfCards
        
        let range1 = CountableRange.init(range)
        self.kolodaView.reloadCardsInIndexRange(range1)
        
        
        
        
    }
    
//    func koloda(kolodaBackgroundCardAnimation koloda: KolodaView) -> POPPropertyAnimation? {
//        let animation = POPSpringAnimation(propertyNamed: kPOPViewFrame)
//        animation?.springBounciness = frameAnimationSpringBounciness
//        animation?.springSpeed = frameAnimationSpringSpeed
//        return animation
//    }
}

// MARK: KolodaViewDataSource
extension FirstViewController: KolodaViewDataSource {
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return DragSpeed.slow
    }
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return self.productInfoArray.count
    }
    
    
    
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {

        
       
        let iv = UIImageView()
        let dict = self.productInfoArray[index]
         self.ProductId = productFirebaseKeys[index]
        let photoUrl = dict.imageUrl
        iv.loadImageWithUrlWithFullSize(urlString: photoUrl)
        return iv
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
        let vw =  Bundle.main.loadNibNamed("CustomOverlayView", owner: self, options: nil)?[0] as? CustomOverlayView
        vw!.setupView(with:self.productInfoArray[index])
        
        
        return vw!
        
        
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .top,
                                relatedBy: .equal,
                                toItem: self.topLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}

