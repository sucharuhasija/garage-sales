//
//  CustomOverlayView.swift
//  Koloda
//
//  Created by Eugene Andreyev on 7/27/15.
//  Copyright (c) 2015 CocoaPods. All rights reserved.
//

import UIKit
import Koloda

private let overlayRightImageName = "overlay_like"
private let overlayLeftImageName = "overlay_skip"

class CustomOverlayView: OverlayView {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var name: UILabel!
  //   @IBOutlet weak var description: UILabel!
    @IBOutlet lazy var overlayImageView: UIImageView! = {
        [unowned self] in
        
        var imageView = UIImageView(frame: self.bounds)
        imageView.backgroundColor = .white
        self.addSubview(imageView)
        self.sendSubview(toBack: imageView)
        return imageView
        }()

    
    func setupView(with snp:Product) {
        
        
        
        self.name.text = snp.name
        self.overlayImageView.backgroundColor = .white
        
        self.productDescription.text = snp.description
        self.priceLabel.text = snp.price
        self.overlayImageView.loadImageWithUrlWithFullSize(urlString: snp.imageUrl)
        
        print(snp.imageUrl)
        
    }
    
    
    
    
    override var overlayState: SwipeResultDirection?  {
        didSet {
            switch overlayState {
            case .left? :
                overlayImageView.image = UIImage(named: overlayLeftImageName)
            case .right? :
                overlayImageView.image = UIImage(named: overlayRightImageName)
            default:
                overlayImageView.image = nil
            }
            
        }
    }

}
