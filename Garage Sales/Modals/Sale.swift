//
//  Sale.swift
//  Garage Sales
//
//  Created by Sucharu hasija on 24/11/17.
//  Copyright © 2017 KW SYSTEMS. All rights reserved.
//

import Foundation



class Sale {
    
    
    var description: String
//    var price: String
//    var imageUrl:String
    var productKey:String
    var latitude:Double
    var longitude:Double
    var userId:String
    var startDate:Date
    var endDate:Date
    var address:String = ""
    var userImageUrl:String = ""
    
    
    
    
    

    init(dict:NSDictionary,withProduct key:String ) {
        
        
        /*
         product["userID"] = self.userUID
         product["startDateAndTime"] = self.startDateAndTime.text!
         product["endDateAndTime"] = self.endDateAndTime.text!
         product["description"] = self.saleDescription.text!
         product["postDate"] = formatter.string(from: Date())
         product["latitude"] = latitude
         product["longitude"] = longitude
         
         */
        
     
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
       // formatter.timeZone = TimeZone(abbreviation: "UTC")
        
        self.userId = dict.value(forKey: "userID") as? String ?? ""
        self.startDate = Date()
        self.endDate = Date()
        self.address = dict.value(forKey: "Address") as? String ?? ""
        
        if let sDateString = dict.value(forKey: "startDateAndTime") as? String, let eDateString =  dict.value(forKey: "endDateAndTime") as? String {
            
            
            self.startDate = formatter.date(from: sDateString)!
            self.endDate = formatter.date(from: eDateString)!
        }
        
        self.description = dict.value(forKey: "description") as? String ?? ""
        self.latitude =  0.0
        self.longitude =  0.0

        
        self.latitude = dict.value(forKey: "latitude") as? Double ?? 0.0
        self.longitude = dict.value(forKey: "longitude") as? Double ?? 0.0
        self.productKey = key
        self.userImageUrl = dict.value(forKey: "ProfileImage") as? String ?? ""
        
        
    }
    
    
    
    
    
}
