//
//  Product.swift
//  Garage Sales
//
//  Created by Keneth Walters on 2017-11-05.
//  Copyright © 2017 KW SYSTEMS. All rights reserved.
//

import Foundation

class Product {
    
    
    var name: String
    var description: String
    var price: String
    var imageUrl:String
    var productKey:String
    var latitude:String
    var longitude:String
    var userId:String
    
    
    
    
    init(name: String, price: String, description: String, imageUrl:String, productKey:String){
        self.name = name
        self.price = price
        self.description = description
        self.imageUrl = imageUrl
        self.productKey = productKey
        self.latitude = ""
        self.longitude = ""
        self.userId = ""
        
        
    }
    
    init(dict:NSDictionary,withProduct key:String ) {
        
        self.userId = dict.value(forKey: "userID") as? String ?? ""
        self.name = dict.value(forKey: "name") as? String ?? ""
        self.price = dict.value(forKey: "price") as? String ?? ""
        self.description = dict.value(forKey: "description") as? String ?? ""
        self.imageUrl = dict.value(forKey: "photoURL") as? String ?? ""
        self.latitude = dict.value(forKey: "latitude") as? String ?? ""
        self.longitude = dict.value(forKey: "longitude") as? String ?? ""
        self.productKey = key
    
    }

    
}
