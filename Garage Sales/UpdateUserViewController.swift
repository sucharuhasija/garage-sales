//
//  UpdateUserViewController.swift
//  Garage Sales
//
//  Created by Keneth Walters on 2017-11-16.
//  Copyright © 2017 KW SYSTEMS. All rights reserved.
//

import UIKit
import Firebase
import NVActivityIndicatorView

class UpdateUserViewController: UIViewController, UITextViewDelegate, UINavigationControllerDelegate,  UIImagePickerControllerDelegate, UITextFieldDelegate, NVActivityIndicatorViewable{

    var imagePicker: UIImagePickerController!
    var imagPicker = UIImagePickerController()
    
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var btnEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var photoUser: UIImageView!
    @IBOutlet weak var txtRetypePassword: UITextField!
     @IBOutlet weak var btnTakePhoto: UIButton!
    
    let refStorage = Storage.storage().reference()
    let refUsers = Database.database().reference()
    
    var name: String = ""
    var phone: String = ""
    
    var userUID = Auth.auth().currentUser!.uid
    var userEmail = Auth.auth().currentUser!.email
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagPicker.delegate = self
        
        // Do any additional setup after loading the view.
        self.btnEmail.text = userEmail
        loadUserInformationFromDatabase()
    }

    
    func loadUserInformationFromDatabase() {
        
        refUsers.child("Users").observe(DataEventType.childAdded, with: {
            
            (snapshot: DataSnapshot) in
         
            
            if snapshot.key == self.userUID {
            
            let info = snapshot.value
            
            print(info ?? "No Value found here")
         
                if info is NSDictionary {
                    
                    
                    self.updateScreenFor(dict: info as! NSDictionary)
                    
                }
                
            // print(self.description1 + self.name)
            
            }
        })
        
    }
    
    func updateScreenFor(dict:NSDictionary) {
        
    
        
        self.txtName.text = dict.value(forKey: "name") as? String
        self.txtPhone.text = dict.value(forKey: "phone") as? String
        self.btnEmail.text = dict.value(forKey: "Email") as? String
        
        if let imageUrl = dict.value(forKey: "photoURL") as? String {
            self.photoUser.loadImageWithUrlWithFullSize(urlString: imageUrl)
        }
        
        
    }
    
    @IBAction func btnSave(_ sender: Any) {
        
            

        
        if txtPassword.text != "" && txtRetypePassword.text != "" {
            
            
            
            
        } else if txtPassword.text != txtRetypePassword.text {
                
           
             
                
            
                let alertController = UIAlertController(title: "Error", message: "Password does not match", preferredStyle: .alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                txtPassword.text = ""
                txtRetypePassword.text = ""
                
                self.present(alertController, animated: true, completion: nil)
        
            
        }else if(txtPassword.text == "" && txtRetypePassword.text != ""){
            let alertController = UIAlertController(title: "Error", message: "Please your password 2 times.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            txtPassword.text = ""
            txtRetypePassword.text = ""
            
            self.present(alertController, animated: true, completion: nil)
            
        }else if(txtPassword.text != "" && txtRetypePassword.text == ""){
            let alertController = UIAlertController(title: "Error", message: "Please your password 2 times.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            txtPassword.text = ""
            txtRetypePassword.text = ""
            
            self.present(alertController, animated: true, completion: nil)
        }
        
      if self.txtName.text == "" || self.txtPhone.text == "" {
        
        
        let alertController = UIAlertController(title: "Error", message: "Please enter an name and phone.", preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
            
       
        }else{
        
        self.updateUserInformationToFirebase()
        
        
        }
    }
       
        
    func updateUserInformationToFirebase() {
        
        
        self.photoUser.image = photoUser.image
        var data = NSData()
        data = UIImageJPEGRepresentation(photoUser.image!, 0.8)! as NSData
        // set upload path
        let filePath = "\(Auth.auth().currentUser!.uid)/\("userPhoto")"
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"
        startAnimating()
        
        
        self.refStorage.child(filePath).putData(data as Data, metadata: metaData){(metaData,error) in
            
            
            
            if let error = error {
                print(error.localizedDescription)
                self.stopAnimating()
                return
            }else{
                //store downloadURL
                let downloadURL = metaData!.downloadURL()!.absoluteString
                //store downloadURL at database
                
                let user  = [                      "userID": self.userUID,
                                                   "name": self.txtName.text!,
                                                   "phone": self.txtPhone.text!,
                                                   "photoURL": downloadURL,
                                                   "Email": self.userEmail ?? ""
                    
                    ] as [String : Any]
                
                self.refUsers.child("Users").child(self.userUID).setValue(user)
                
                UserDefaults.standard.set(true, forKey: "isUserPhoneAvailable")
                UserDefaults.standard.set(true, forKey: "isUserEmailAvailable")
                
                self.updateUserPassword()
                print("llego aqui")
                
                
            }
            
        }
        
    }
    func updateUserPassword() {
        
        Auth.auth().currentUser?.updatePassword(to: txtPassword.text!) { (error) in
            
            
            self.stopAnimating()
            if error == nil {
                
                let alertController = UIAlertController(title: "Error", message: "Please type your password again", preferredStyle: .alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.txtPassword.text = ""
                self.txtRetypePassword.text = ""
            }
        }
        
        
        
    }
    
 

    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagPicker = UIImagePickerController()
            imagPicker.delegate = self
            imagPicker.sourceType = .camera
            present(imagPicker, animated: true, completion: nil)
            
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagPicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagPicker.allowsEditing = true
        self.present(imagPicker, animated: true, completion: nil)
       
    }
 
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagPicker.dismiss(animated: true, completion: nil)
        photoUser.image = info[UIImagePickerControllerOriginalImage] as? UIImage
    }
    
    
    @IBAction func btnTakePhoto(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
        
    }
}
