//
//  ProductTableViewCell.swift
//  Garage Sales
//
//  Created by Sucharu hasija on 24/11/17.
//  Copyright © 2017 KW SYSTEMS. All rights reserved.
//

import UIKit
import Firebase
import SwiftGifOrigin


protocol ProductTableActionProtocol:class {
    
    func callPressedByUser(with ind:Int)
    func emailPressedByUser(with ind:Int)
    
    
}

class ProductTableViewCell: UITableViewCell {

    
    @IBOutlet weak var saleDescription: UILabel!
    @IBOutlet weak var saleTime: UILabel!
    @IBOutlet weak var saleAddress: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    
    
    weak var dele:ProductTableActionProtocol? = nil
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
   
        // Initialization code
    }

    func setupCellInformation(with dict:Sale, forIndex path:Int) {
        
     
        self.saleAddress.numberOfLines = 0
        self.saleAddress.text = dict.address
        self.saleDescription.text = dict.description
        
        print(dict.address,dict.description)
        self.userImageView.image = UIImage(named:"dummy")
        self.userImageView.loadImageWithUrlWithFullSize(urlString: dict.userImageUrl)
        
        let frm = DateFormatter()
        frm.dateFormat = "yyyy/MM/dd'-'hh:mm"
        
        
        self.saleTime.text = "From: \(frm.string(from: dict.startDate)) to \(frm.string(from: dict.endDate))"
        
        
        
        
    }
    

    @IBAction func emailButtonPressed(_ sender: UIButton) {
    
        dele?.emailPressedByUser(with: sender.tag)
    
    }
    
    
    
    @IBAction func callPressed(_ sender: UIButton) {
        
        dele?.callPressedByUser(with: sender.tag)
    }
}
