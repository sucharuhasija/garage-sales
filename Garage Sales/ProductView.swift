//
//  ProductView.swift
//  Garage Sales
//
//  Created by Sucharu hasija on 13/11/17.
//  Copyright © 2017 KW SYSTEMS. All rights reserved.
//

import UIKit
import Firebase
import Koloda

class ProductView: OverlayView {

    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        super.draw(rect)
    }
    
    func setupView(with snp:NSDictionary) {
        
        
        
        self.productName.text = snp.value(forKey: "name") as? String
        self.productDescription.text = snp.value(forKey: "description") as? String
        
        if let imageUrl = snp.value(forKey: "photoURL") as? String {
            
            self.productImageView.loadImageWithUrlWithFullSize(urlString: imageUrl)

        }

    }

}
