//
//  SignUpViewController.swift
//  Garage Sales
//
//  Created by Keneth Walters on 2017-11-02.
//  Copyright © 2017 KW SYSTEMS. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    let refUsers = Database.database().reference()

    @IBAction func btnSignUp(_ sender: Any) {
    
        if txtName.text == "" {
            
            self.showWarningMessage(with: "Please enter your email.")
     
        } else if txtPassword.text == "" {
            
            self.showWarningMessage(with: "Please enter your password.")
            
        } else if txtName.text == "" {
            
            self.showWarningMessage(with: "Please enter your Phone Number")
            
        }
         else {
            Auth.auth().createUser(withEmail: txtName.text!, password: txtPassword.text!) { (user, error) in
                
                if error == nil {
                    print("You have successfully signed up")
                    self.updateUserValuesInFirebaseStore()
                    
               
                } else {
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
        
        
    }
    
    func updateUserValuesInFirebaseStore() {
        
        let userID = Auth.auth().currentUser!.uid
        
        let user  = [                      "userID": userID,
                                           "name": "",
                                           "phone": self.txtPhone.text!,
                                           "photoURL": "",
                                           "Email":  ""
            
            ] as [String : Any]
        
        self.refUsers.child("Users").child(userID).setValue(user)

        
        self.GoToNewShoot()
        
    }
    
    
    func GoToNewShoot(){
        
        
        let alertController = UIAlertController(title: "Good!", message: "You have successfully signed up", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            //run your function here
            print("Method Called")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "InitialViewController")
            self.present(vc!, animated: true, completion: nil)
            
        }))
        self.present(alertController, animated: true, completion: nil)
        
     
    }
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
        
    }

}
