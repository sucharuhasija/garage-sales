//
//  TRLocationManager.swift
//  EATA
//
//  Created by Sucharu Hasija on 31/08/16.
//  Copyright © 2016 Sucharu Hasija. All rights reserved.
//

import Foundation
import CoreLocation


class TRLocationManager : NSObject, CLLocationManagerDelegate
{
    
    
    var locationManager:CLLocationManager!
    var cH:((NSDictionary) -> Void)?
    
    class var swiftSharedInstance: TRLocationManager {
        struct Singleton {
            static let instance = TRLocationManager()
            
        }
        return Singleton.instance
    }
    // General Method to get user Location in Completion Handler
    
    
    
    
    
    
    func getUserLocation(completionHandler:@escaping (_ locationDetail:NSDictionary)->Void)
    {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        self.cH = completionHandler
        
        
    }
    func getDistanceBetweenMyLocationAndGivenLocation(with lat:Double , andLongitude long:Double) -> Double {
        
        let myLocation = CLLocation(latitude: lat, longitude: long)
        
        
        if let lat1  = UserDefaults.standard.value(forKey: "latitude") as? String, let long1 = UserDefaults.standard.value(forKey: "longitude") as? String {
            
                let nextLocation = CLLocation(latitude: Double(lat1) ?? 0.0, longitude: Double(long1) ?? 0.0)
            
             return   myLocation.distance(from: nextLocation)
            
        }else {
            
             return 0.0
            
        }
       
 
        
        
        
        
        
        
    }
    
    func isLocationPermissionEnabled() -> Bool {
        
        
        return CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse
        
        
        
        
    }
    
    // CLLocation Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        
        let location = locations.last! as CLLocation
        let dft = UserDefaults.standard
        
        
        
        dft.set("\(location.coordinate.latitude)", forKey: "latitude")
        dft.set("\(location.coordinate.longitude)", forKey: "longitude")
        dft.synchronize()
        
        
        locationManager.stopUpdatingLocation()
        
        
        // Reverse GeoCoding for Address //
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error)->Void in
            
            if error != nil {
                // print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if placemarks!.count > 0 {
                
                let pm = placemarks![0]
                let someDict=pm.addressDictionary
                
                
                if self.cH != nil
                {
                    
                    self.cH!(someDict! as NSDictionary)
                    
                }
                
            }
            else {
                // print("Problem with the data received from geocoder")
            }
        })
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }

    
    
    
    
    
    
    
}
