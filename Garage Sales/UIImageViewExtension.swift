//
//  UIImageViewExtension.swift
//  Garage Sales
//
//  Created by Sucharu hasija on 13/11/17.
//  Copyright © 2017 KW SYSTEMS. All rights reserved.
//

import Foundation
import SDWebImage
import SwiftGifOrigin



extension UIImageView
{
    
    
    func loadImageWithUrlWithFullSize(urlString:String)
    {
        
        
        guard let url = URL(string: urlString) else { return }
        
        
        let image:UIImage! = UIImage.gif(name: "spinner")
        self.backgroundColor = .white

       // let image:UIImage! = UIImage(named:"DummyImage")
        
        self.sd_setImage(with: url, placeholderImage: image, options: SDWebImageOptions.continueInBackground)  { (image, bubble, _, _) in
            
            
            if image != nil
            {
                SDImageCache.shared().store(image, forKey: url.absoluteString)
                self.image = image
                 self.contentMode = UIViewContentMode.scaleToFill
                
            }
  
            
        }
        
        
    }
    
    
    
}
