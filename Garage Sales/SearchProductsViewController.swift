//
//  SearchProductsViewController.swift
//  Garage Sales
//
//  Created by Sucharu hasija on 24/11/17.
//  Copyright © 2017 KW SYSTEMS. All rights reserved.
//

import UIKit
import Firebase
import GooglePlaces
import NVActivityIndicatorView


class SearchProductsViewController: UIViewController, UITextViewDelegate, NVActivityIndicatorViewable {

    @IBOutlet weak var createSaleButton: UIButton!
    @IBOutlet weak var adView: GADBannerView!
    @IBOutlet weak var selectAddress: UITextField!
    @IBOutlet weak var saleDescription: UITextView!
    @IBOutlet weak var endDateAndTime: UITextField!
    @IBOutlet weak var startDateAndTime: UITextField!
    var userUID = Auth.auth().currentUser!.uid

    let refProducts = Database.database().reference()

    var datePicker = UIDatePicker()
    var latitude:Double = 0.0
    var longitude:Double = 0.0
    var profileImage = ""
    var isComingForViewOnly = false
    var updateInfo:Sale? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        self.startDateAndTime.tag = 102
        self.endDateAndTime.tag = 103
        self.startDateAndTime.inputView = datePicker
        self.endDateAndTime.inputView = datePicker
        self.startDateAndTime.delegate = self
        self.endDateAndTime.delegate = self
        self.selectAddress.delegate = self

        self.saleDescription.delegate = self
        self.saleDescription.layer.masksToBounds = true
        self.saleDescription.layer.cornerRadius = 6.0
        self.saleDescription.layer.borderColor = UIColor.lightGray.cgColor
        self.saleDescription.layer.borderWidth = 1.0
        
        adView.adUnitID = "ca-app-pub-0187834556320329/8322934772"
        adView.rootViewController = self
        adView.load(GADRequest())
        
        if let lat  = UserDefaults.standard.value(forKey: "latitude") as? String, let long = UserDefaults.standard.value(forKey: "longitude") as? String {
            
            self.latitude = Double(lat) ?? 0.0
                self.longitude = Double(long) ?? 0.0
            
        }
        
        
        if isComingForViewOnly {
            
            
            updateScreenForUserView()
        }
        
        self.datePicker.minimumDate = Date()
        
        TRLocationManager.swiftSharedInstance.getUserLocation { (_) in
            
        }
      
        
    }
    func updateScreenForUserView() {
        
        
        self.saleDescription.isUserInteractionEnabled = false
        self.startDateAndTime.isUserInteractionEnabled = false
        self.endDateAndTime.isUserInteractionEnabled = false
        self.selectAddress.isUserInteractionEnabled = false
        self.createSaleButton.isHidden = true
        self.saleDescription.text = updateInfo!.description
        self.selectAddress.text = updateInfo!.address
        self.navigationItem.leftBarButtonItem = nil
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        self.startDateAndTime.text = formatter.string(from: updateInfo!.startDate)
        self.endDateAndTime.text = formatter.string(from: updateInfo!.endDate)
        
        
        
    }

    func getUserProfileInfo() {
        
        
        self.refProducts.child("Users").child(self.userUID).child("photoURL").observe(DataEventType.value) { (snapshot) in
            
            
            print(snapshot.value ?? "No value exists")
            if let  x = snapshot.value , x as? NSObject != NSNull() {
                
                
              self.profileImage = x as? String ?? ""
            }
        }
        
        
        
    }

    
    @IBAction func createSalePressed(_ sender: UIButton) {
        
        
        
        if self.startDateAndTime.text!.isEmpty {
            
            self.showWarningMessage(with: "Please select Start Date and Time")
            
        } else if self.endDateAndTime.text!.isEmpty {
            
            self.showWarningMessage(with: "Please select End Date and Time")
            
        } else if self.saleDescription.text!.isEmpty {
            
            self.showWarningMessage(with: "Please add Sale Description. ")
            
        } else if self.saleDescription.text!.isEmpty {
            
            self.showWarningMessage(with: "Please add Address.")
            
        } else {
            
            self.addInformationToSales()
            
        }
        
        
        
        
    }
    
    
    
    func addInformationToSales() {
        
    
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        
 
  

                var product = [String:Any]()
                product["userID"] = self.userUID
                product["startDateAndTime"] = self.startDateAndTime.text!
                product["endDateAndTime"] = self.endDateAndTime.text!
                product["description"] = self.saleDescription.text!
                product["postDate"] = formatter.string(from: Date())
                product["latitude"] = self.latitude 
                product["longitude"] = self.longitude
                product["NotInterested"] = self.userUID
                product["Address"] = self.selectAddress.text!
                  product["ProfileImage"]     = profileImage

            self.startAnimating()
        self.refProducts.child("Sales").childByAutoId().setValue(product) { (_, _) in
            
            self.stopAnimating()
            self.showSuccessMessage()
          
            
            
            
        }
                
        
 
        
    }
    
    func showSuccessMessage() {
        
        let alert = UIAlertController(title: "Success", message: "Your sale is On!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        self.present(alert, animated: true)
        
        
        self.saleDescription.text = ""
        self.selectAddress.text = ""
        self.startDateAndTime.text = ""
        self.endDateAndTime.text = ""
    }
    
    @IBAction func canPressed(_ sender: UIBarButtonItem) {
        
        self.tabBarController?.dismiss(animated: true, completion: nil)
        
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Enter Description"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func openGooglePlaces() {
        
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
        
        
    }

}



extension SearchProductsViewController:UITextFieldDelegate {
    
    

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        if textField.tag == 102 {
            
            self.datePicker.date = Date()
            
        } else if textField.tag == 103 {
            
            
            self.datePicker.date = Date().addingTimeInterval(60 * 60 * 24)
            
        } else if textField.tag == 104 {
            
            
            self.openGooglePlaces()
            
            
        }
        
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        
        
        if textField.tag == 102 {
          
            
            textField.text = formatter.string(from: self.datePicker.date)
            
        } else if textField.tag == 103 {
            
            
            textField.text = formatter.string(from: self.datePicker.date)
        }
  
    }
    
    
}
extension SearchProductsViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
     //   print("Place attributions: \(place.attributions)")
        
        
        self.latitude = place.coordinate.latitude
        self.longitude = place.coordinate.longitude
        
        self.selectAddress.text = place.formattedAddress ?? ""
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
