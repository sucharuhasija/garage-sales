//
//  LogInViewController.swift
//  Garage Sales
//
//  Created by Keneth Walters on 2017-11-02.
//  Copyright © 2017 KW SYSTEMS. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import NVActivityIndicatorView
import GoogleMobileAds

class LogInViewController: UIViewController, FBSDKLoginButtonDelegate, NVActivityIndicatorViewable, UITextFieldDelegate {
    
    @IBOutlet weak var txtEmailLogin: UITextField!
   
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var btnFacebook: FBSDKLoginButton!

    @IBOutlet weak var bannerView: GADBannerView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
     self.btnFacebook.delegate = self
     self.btnFacebook.readPermissions = ["email"]
        
//        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
//        
//        addBannerViewToView(bannerView)
        
        bannerView.adUnitID = "ca-app-pub-0187834556320329/7075579518"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
       // bannerView.delegate = self
    }
    
    
    func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool {
        
        return true

    }
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
        
        if !result.isCancelled {
            self.startAnimating()
            let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
            Auth.auth().signIn(with: credential) { (user, error) in
                
                self.stopAnimating()
                            if let error = error {
                                print(error)
                                return
                            }
                
                //WARNING: - NEED TO CONNECT FIREBASE WITH FACEBOOK FIRST
                // User is signed in
                // ...
                //Go to the HomeViewController if the login is sucessful
                
                 FBSDKLoginManager().logOut()
                 let vc = self.storyboard?.instantiateViewController(withIdentifier: "InitialViewController")
                 self.present(vc!, animated: true, completion: nil)
                UserDefaults.standard.set(true, forKey: "isLogin")
                
            }
            //Go to the HomeViewController if the login is sucessful
        
            
            
        } else {
            
            
            
            
        }

    }
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
    }
  
    @IBAction func btnActionFacebook(_ sender: Any) {
        
    }
    @IBAction func btnLogin(_ sender: Any) {
        
      
        
        if self.txtEmailLogin.text == "" || self.txtPassword.text == "" {
            
            //Alert to tell the user that there was an error because they didn't fill anything in the textfields because they didn't fill anything in
            
            let alertController = UIAlertController(title: "Error", message: "Please enter an email and password.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            self.startAnimating()
            Auth.auth().signIn(withEmail: self.txtEmailLogin.text!, password: self.txtPassword.text!) { (user, error) in
                self.stopAnimating()

                if error == nil {
                    
                    //Print into the console if successfully logged in
                    print("You have successfully logged in")
                    
                    //Go to the HomeViewController if the login is sucessful
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "InitialViewController")
                    self.present(vc!, animated: true, completion: nil)
                    UserDefaults.standard.set(true, forKey: "isLogin")
                    
                } else {
                    
                    //Tells the user that there is an error and then gets firebase to tell them the error
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }

 
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
        
    }

}
